//
//  AppDelegate.swift
//  RoastMad
//
//  Created by Adam Gall on 4/5/16.
//  Copyright © 2016 AdamGall. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        return true
    }
}

