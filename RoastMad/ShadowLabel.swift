//
//  ShadowLabel.swift
//  RoastMad
//
//  Created by Adam Gall on 4/6/16.
//  Copyright © 2016 AdamGall. All rights reserved.
//

import Foundation
import UIKit

class ShadowLabel: UILabel {
    
    override func drawTextInRect(rect: CGRect) {
        let shadowOffset = self.shadowOffset
        let textColor = UIColor.ghostWhiteColor()
        
        let c = UIGraphicsGetCurrentContext()
        CGContextSetLineWidth(c, 1)
        CGContextSetLineJoin(c, .Round)
        
        CGContextSetTextDrawingMode(c, .Stroke)
        self.textColor = UIColor.black25PercentColor()
        super.drawTextInRect(rect)
        
        CGContextSetTextDrawingMode(c, .Fill)
        self.textColor = textColor
        self.shadowOffset = CGSizeMake(0, 0)
        super.drawTextInRect(rect)
        
        self.shadowOffset = shadowOffset
    }
    
}
