//
//  ViewController.swift
//  RoastMad
//
//  Created by Adam Gall on 4/5/16.
//  Copyright © 2016 AdamGall. All rights reserved.
//

import Colours
import UIKit

class ViewController: UIViewController {

    @IBOutlet var quoteLabel: UILabel?
    var inspirationalQuotes: Array<String> {
        get {
            // from http://www.inspirational-quotes.info/motivational-quotes.html
            return [
            "We are what we repeatedly do. Excellence, therefore, is not an act but a habit.",
            "Take calculated risks. That is quite different from being rash.",
            "Storms make oaks take roots.",
            "If you do not hope, you will not find what is beyond your hopes.",
            "Dream big and dare to fail.",
            "With will one can do anything.",
            "We are all inventors, each sailing out on a voyage of discovery, guided each by a private chart, of which there is no duplicate. The world is all gates, all opportunities.",
            "When you get into a tight place and everything goes against you, till it seems you could not hang on a minute longer, never give up then, for that is just the place and time that the tide will turn.",
            "Seek the lofty by reading, hearing and seeing great work at some moment every day.",
            "The only way of finding the limits of the possible is by going beyond them into the impossible.",
            "Well begun is half done.",
            "The best way out is always through.",
            "Without inspiration the best powers of the mind remain dormant. There is a fuel in us which needs to be ignited with sparks.",
            "Life consists not in holding good cards, but in playing those you hold well.",
            "He who moves not forward goes backward.",
            "And all may do what has by man been done.",
            "Hope is like the sun, which, as we journey toward it, casts the shadow of our burden behind us.",
            "Work spares us from three evils: boredom, vice, and need.",
            "If the wind will not serve, take to the oars.",
            "Men's best successes come after their disappointments.",
            "No star is lost once we have seen, We always may be what we might have been.",
            "He turns not back who is bound to a star.",
            "You cannot plough a field by turning it over in your mind.",
            "Do not wait to strike till the iron is hot; but make it hot by striking.",
            "Strong lives are motivated by dynamic purposes.",
            "Nothing will ever be attempted if all possible objections must first be overcome.",
            "Fortune favors the brave.",
            "When the best things are not possible, the best may be made of those that are.",
            "He who hesitates is lost.",
            "If you want to succeed in the world must make your own opportunities as you go on. The man who waits for some seventh wave to toss him on dry land will find that the seventh wave is a long time a coming. You can commit no greater folly than to sit by the roadside until some one comes along and invites you to ride with him to wealth or influence.",
            "An oak is not felled at one blow.",
            "In doubtful matters boldness is everything.",
            "Great spirits have always encountered violent opposition from mediocre minds.",
            "Amazing how we can light tomorrow with today.",
            "In every difficult situation is potential value. Believe this, then begin looking for it.",
            "Believe with all of your heart that you will do what you were made to do.",
            "Each day I look for a kernel of excitement. In the morning, I say: \"What is my exciting thing for today?\" Then I do the day.",
            "Knowing is not enough; we must apply. Willing is not enough; we must do.",
            "We are still masters of our fate. We are still captains of our souls.",
            "Nothing great was ever achieved without enthusiasm.",
            "Reach perfection.",
            "For hope is but the dream of those that wake.",
            "Constant dripping hollows out a stone.",
            "Nothing contributes so much to tranquilize the mind as a steady purpose--a point on which the soul may fix its intellectual eye."
            ]
        }
    }
    var currentQuote: String!
    var allBackgroundBaseColors: Array<Color> {
        get {
            return [
                UIColor.grapefruitColor(),
                UIColor.turquoiseColor(),
                UIColor.emeraldColor(),
                UIColor.grassColor(),
                UIColor.salmonColor(),
                UIColor.chartreuseColor(),
                UIColor.maroonColor(),
                UIColor.palePurpleColor(),
                UIColor.grapeColor(),
                UIColor.periwinkleColor(),
                UIColor.bananaColor(),
                UIColor.peachColor(),
                UIColor.burntSiennaColor()
            ]
        }
    }
    var currentBaseColor: Color!
    var currentBackgroundColors: Array<Color>!
    var currentColor: Color!
    
    // MARK: Initializers
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        currentQuote = ""
        currentBaseColor = allBackgroundBaseColors.first
        currentColor = currentBaseColor
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ViewController.foregrounding), name: UIApplicationDidBecomeActiveNotification, object: nil)
    }
    
    // MARK: UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        refreshView(true)
    }
    
    // MARK: Actions
    
    @IBAction func tappedScreen(sender: UIView) {
        refreshView(false)
    }
    
    // MARK:
    // MARK: Private helpers
    
    func foregrounding() {
        refreshView(true)
    }
    
    func refreshView(newBase: Bool) {
        if newBase {
            currentBaseColor = getNewCurrentBaseColor()
            currentBackgroundColors = currentBaseColor.colorScheme(.Analagous)
        }
        view.backgroundColor = getNewBackgroundColor()
        quoteLabel!.text = getNewQuote()
    }
    
    // MARK: Base color randomizers
    
    func getNewCurrentBaseColor() -> Color {
        var newBaseColor = getRandomBaseColor()
        if newBaseColor == currentBaseColor {
            newBaseColor = getNewCurrentBaseColor()
        }
        currentBaseColor = newBaseColor
        return newBaseColor
    }
    
    func getRandomBaseColor() -> Color {
        return allBackgroundBaseColors[Int(arc4random_uniform(UInt32(allBackgroundBaseColors.count)))]
    }
    
    // MARK: Background color randomizers
    
    func getNewBackgroundColor() -> Color {
        var newColor = getRandomColor()
        if newColor == currentColor {
            newColor = getNewBackgroundColor()
        }
        currentColor = newColor
        return newColor
    }
    
    func getRandomColor() -> Color {
        return currentBackgroundColors[Int(arc4random_uniform(UInt32(currentBackgroundColors.count)))]
    }
    
    // MARK: Quote randomizers
    
    func getNewQuote() -> String {
        var newQuote = getRandomQuote()
        if newQuote == currentQuote {
            newQuote = getNewQuote()
        }
        currentQuote = newQuote
        return newQuote
    }
    
    func getRandomQuote() -> String {
        return inspirationalQuotes[Int(arc4random_uniform(UInt32(inspirationalQuotes.count)))]
    }
    
}
